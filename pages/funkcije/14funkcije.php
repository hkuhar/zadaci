<?php include $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/include.php"; ?>
<?php
$ocjene = [1 => 'nedovoljan 1', 2 => 'dovoljan 2', 3 => 'dobar 3', 4 => 'vrlo dobar 4', 5 => 'izvrstan 5'];
echo "------samo implode------<br>";
echo implode($ocjene);
echo "<br>------prošireni implode 1------<br>";
echo implode(", ", $ocjene);
echo "<br>------prošireni implode 2------<br>";
echo implode(" <b>nešto</b> ", $ocjene);
echo "<br>------prošireni implode 3------<br>";
echo "<b>prije</b> " . implode(" <b>nešto</b> ", $ocjene) . " <b>poslije</b>";

echo "<br><br>Obična lista<br>";
echo "<ul><li>" . implode("</li><li>", $ocjene) . "</li></ul>";
echo "Drop down lista<br>";
echo "<select><option>" . implode("</opiton><option>", $ocjene) . "</option></select><br>";


$array = ["ime" => "Marko", 
          "prezime" => "Marković",
          "email" => "mmarkovic@vub.hr"];
//kreiram strnig za insert
 $sql  = "INSERT INTO korisnici";

 // key-evi
 $sql .= " ('".implode("', '", array_keys($array))."')";

 // values 
 $sql .= " VALUES ('".implode("', '", $array)."') ";
 echo "sql<br> " . $sql;

?>

</div>
        <div class="col-xs-6">
            <?php echo "<b>" . __FILE__ . "</b><br>"; highlight_file(__FILE__);?>
        </div>
    </div>
</div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/footer.php"; ?>