<?php include $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/include.php"; ?>
<?php
//1 funkcija 
  function sayHello1(){
    echo "Hello World!<br>";
  }
//2. funkcija
  function sayHello2($word){
    echo "Hello {$word}!<br>";
  }
//3. funkcija
  function say_hello3($pozdrav, $tko, $znak){
     echo $pozdrav . ", " . $tko . $znak . "<br>";
  }
//4. funkcija
  function zbrajanje4($val1, $val2){
     $sum = $val1 + $val2;
     return $sum;
  }    
//5. funkcija
  function zbrojOduzmi($val1, $val2){
     $zbroj = $val1 + $val2;
     $razlika = $val1 - $val2;
     $rezultat = array($zbroj, $razlika);
     return $rezultat; 	 
  }
//6. funkcije
function kojeBoje($boja = "bijele", $soba = "Kuhinja"){
     echo "{$soba} je {$boja} boje.<br>";
  }
   
  //---------1
  echo "<b>1. funkcija</b><br>"; 
  sayHello1();
  //---------2
  echo "<b>2. funkcija</b><br>"; 
  sayHello2("World");
  sayHello2("Everyone");
  $name = "Marko Markovic";
  sayHello2($name);
  //---------3
  echo "<b>3. funkcija</b><br>"; 
  say_hello3("Bok", $name, "!");
  //---------4
  echo "<b>4. funkcija</b><br>"; 
  $num1 = 11;
  $num2 = 539;
  echo "Suma brojeva {$num1} i {$num2} = " . zbrajanje4($num1,$num2) . "<br>";
  if (zbrajanje4(5,6) == 11){
     echo "Da, super<br>";
  }
  //-----------5
  echo "<b>5. funkcija</b><br>"; 
  $rezultat = zbrojOduzmi(25, 18);
  echo "Zbroj 25 i 18: " . $rezultat[0] . "<br>";
  echo "razlika 25 i 18: " . $rezultat[1] . "<br>";
  //-----------6
  echo "<b>6. funkcija</b><br>"; 
  kojeBoje ("plave");
  kojeBoje ("zelene", "Radna soba");  
?>
<br>

</div>
        <div class="col-xs-6">
            <?php echo "<b>" . __FILE__ . "</b><br>"; highlight_file(__FILE__);?>
        </div>
    </div>
</div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/footer.php"; ?>

