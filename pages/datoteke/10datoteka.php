<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/include.php"; ?>
    <h1>Stranica 10</h1>
    <p> Nalazimo se na stranici 10</p>	
	
    <form action="" method="post" enctype="multipart/form-data">
        <h2>Upload csv</h2>
        <label for="fileSelect">Filename:</labe>
        <input type="file" name="datoteka" id="fileSelect">
        <input type="submit" name="submit" value="Upload">
        <p><strong>Note:</strong> Samo su .csv datoteke dozvoljene maksimalne vrijednosti.</p>
    </form>	
	
<?php 

chdir("../..");

if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Check if file was uploaded without errors
    if(isset($_FILES["datoteka"]) && $_FILES["datoteka"]["error"] == 0){
        
		$allowed = array("csv" => "application/vnd.ms-excel");
        
		$filename = $_FILES["datoteka"]["name"];
        $filetype = $_FILES["datoteka"]["type"];
        $filesize = $_FILES["datoteka"]["size"];
		    
        // Provjera extenzije datoteke
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
				
        if(!array_key_exists($ext, $allowed)) die("Molimo odaberite datoteku s validnim formatom.");
    
        // Provjera veličine datoteke - 5MB maximum
        $maxsize = 5 * 1024 * 1024;
        if($filesize > $maxsize) die("Datoteka ne smije biti veća od dozvoljenog limita.");
    
        // Provjera MIME-a vrijednost u allowed polju
        if(in_array($filetype, $allowed)){
            // Provjera da li datoteka već postoji
            if(file_exists("files/" . $filename)){
                echo $filename . " datoteka već postoji.";
            } else{
                move_uploaded_file($_FILES["datoteka"]["tmp_name"], "files/" . $filename);
                echo "Datoteka je uspješno učitana.";
            } 
			echo prikazCsv("./files/$filename");
        } else{
            echo "Datoteku nije moguće učitati, molim javite se u Informatiku"; 
        }
    } else{
        echo "Greška: " . $_FILES["datoteka"]["error"];
    }
}
?>	

</div>
        <div class="col-xs-6">
            <?php echo "<b>" . __FILE__ . "</b><br>"; highlight_file(__FILE__);?>
        </div>
    </div>
</div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/footer.php"; ?>
